//stamp:0b17d09586a027a0
/*<------------------------------------------------------------------------------------------------->*/
/*该文件由uiresbuilder生成，请不要手动修改*/
/*<------------------------------------------------------------------------------------------------->*/
#pragma once
#include <res.mgr/snamedvalue.h>
#define ROBJ_IN_CPP \
namespace SOUI \
{\
    const _R R;\
    const _UIRES UIRES;\
}
namespace SOUI
{
	class _UIRES{
		public:
		class _UIDEF{
			public:
			_UIDEF(){
				XML_INIT = _T("UIDEF:XML_INIT");
			}
			const TCHAR * XML_INIT;
		}UIDEF;
		class _LAYOUT{
			public:
			_LAYOUT(){
				XML_MAINWND = _T("LAYOUT:XML_MAINWND");
				dlg_input = _T("LAYOUT:dlg_input");
				dlg_sel_col = _T("LAYOUT:dlg_sel_col");
			}
			const TCHAR * XML_MAINWND;
			const TCHAR * dlg_input;
			const TCHAR * dlg_sel_col;
		}LAYOUT;
		class _values{
			public:
			_values(){
				string = _T("values:string");
				color = _T("values:color");
				skin = _T("values:skin");
			}
			const TCHAR * string;
			const TCHAR * color;
			const TCHAR * skin;
		}values;
		class _IMG{
			public:
			_IMG(){
			}
		}IMG;
		class _ICON{
			public:
			_ICON(){
				ICON_LOGO = _T("ICON:ICON_LOGO");
			}
			const TCHAR * ICON_LOGO;
		}ICON;
	};
	const SNamedID::NAMEDVALUE namedXmlID[]={
		{L"_name_start",65535},
		{L"btn_close",65536},
		{L"btn_delete",65541},
		{L"btn_insert",65540},
		{L"btn_max",65537},
		{L"btn_min",65539},
		{L"btn_modify",65542},
		{L"btn_ok",65578},
		{L"btn_restore",65538},
		{L"btn_sel_col",65543},
		{L"btn_test",65545},
		{L"btn_tv_test",65546},
		{L"chk_age",65580},
		{L"chk_gender",65579},
		{L"chk_hobby",65581},
		{L"chk_name",65558},
		{L"chk_sel",65556},
		{L"col_age",65562},
		{L"col_data1",65566},
		{L"col_data2",65568},
		{L"col_data3",65570},
		{L"col_data4",65572},
		{L"col_gender",65560},
		{L"col_hobby",65564},
		{L"col_name",65557},
		{L"container",65549},
		{L"edit_age",65576},
		{L"edit_gender",65575},
		{L"edit_hobby",65577},
		{L"edit_name",65574},
		{L"mclv_header",65548},
		{L"mclv_test",65547},
		{L"settingStyle_coWidth",65554},
		{L"settingStyle_customtitle",65553},
		{L"settingStyle_index",65551},
		{L"settingStyle_listview",65550},
		{L"settingStyle_title",65552},
		{L"settingStyle_visible",65555},
		{L"treeveiw_test",65544},
		{L"txt_age",65563},
		{L"txt_data1",65567},
		{L"txt_data2",65569},
		{L"txt_data3",65571},
		{L"txt_data4",65573},
		{L"txt_gender",65561},
		{L"txt_hobby",65565},
		{L"txt_name",65559}		};
	class _R{
	public:
		class _name{
		public:
		_name(){
			_name_start = namedXmlID[0].strName;
			btn_close = namedXmlID[1].strName;
			btn_delete = namedXmlID[2].strName;
			btn_insert = namedXmlID[3].strName;
			btn_max = namedXmlID[4].strName;
			btn_min = namedXmlID[5].strName;
			btn_modify = namedXmlID[6].strName;
			btn_ok = namedXmlID[7].strName;
			btn_restore = namedXmlID[8].strName;
			btn_sel_col = namedXmlID[9].strName;
			btn_test = namedXmlID[10].strName;
			btn_tv_test = namedXmlID[11].strName;
			chk_age = namedXmlID[12].strName;
			chk_gender = namedXmlID[13].strName;
			chk_hobby = namedXmlID[14].strName;
			chk_name = namedXmlID[15].strName;
			chk_sel = namedXmlID[16].strName;
			col_age = namedXmlID[17].strName;
			col_data1 = namedXmlID[18].strName;
			col_data2 = namedXmlID[19].strName;
			col_data3 = namedXmlID[20].strName;
			col_data4 = namedXmlID[21].strName;
			col_gender = namedXmlID[22].strName;
			col_hobby = namedXmlID[23].strName;
			col_name = namedXmlID[24].strName;
			container = namedXmlID[25].strName;
			edit_age = namedXmlID[26].strName;
			edit_gender = namedXmlID[27].strName;
			edit_hobby = namedXmlID[28].strName;
			edit_name = namedXmlID[29].strName;
			mclv_header = namedXmlID[30].strName;
			mclv_test = namedXmlID[31].strName;
			settingStyle_coWidth = namedXmlID[32].strName;
			settingStyle_customtitle = namedXmlID[33].strName;
			settingStyle_index = namedXmlID[34].strName;
			settingStyle_listview = namedXmlID[35].strName;
			settingStyle_title = namedXmlID[36].strName;
			settingStyle_visible = namedXmlID[37].strName;
			treeveiw_test = namedXmlID[38].strName;
			txt_age = namedXmlID[39].strName;
			txt_data1 = namedXmlID[40].strName;
			txt_data2 = namedXmlID[41].strName;
			txt_data3 = namedXmlID[42].strName;
			txt_data4 = namedXmlID[43].strName;
			txt_gender = namedXmlID[44].strName;
			txt_hobby = namedXmlID[45].strName;
			txt_name = namedXmlID[46].strName;
		}
		 const wchar_t * _name_start;
		 const wchar_t * btn_close;
		 const wchar_t * btn_delete;
		 const wchar_t * btn_insert;
		 const wchar_t * btn_max;
		 const wchar_t * btn_min;
		 const wchar_t * btn_modify;
		 const wchar_t * btn_ok;
		 const wchar_t * btn_restore;
		 const wchar_t * btn_sel_col;
		 const wchar_t * btn_test;
		 const wchar_t * btn_tv_test;
		 const wchar_t * chk_age;
		 const wchar_t * chk_gender;
		 const wchar_t * chk_hobby;
		 const wchar_t * chk_name;
		 const wchar_t * chk_sel;
		 const wchar_t * col_age;
		 const wchar_t * col_data1;
		 const wchar_t * col_data2;
		 const wchar_t * col_data3;
		 const wchar_t * col_data4;
		 const wchar_t * col_gender;
		 const wchar_t * col_hobby;
		 const wchar_t * col_name;
		 const wchar_t * container;
		 const wchar_t * edit_age;
		 const wchar_t * edit_gender;
		 const wchar_t * edit_hobby;
		 const wchar_t * edit_name;
		 const wchar_t * mclv_header;
		 const wchar_t * mclv_test;
		 const wchar_t * settingStyle_coWidth;
		 const wchar_t * settingStyle_customtitle;
		 const wchar_t * settingStyle_index;
		 const wchar_t * settingStyle_listview;
		 const wchar_t * settingStyle_title;
		 const wchar_t * settingStyle_visible;
		 const wchar_t * treeveiw_test;
		 const wchar_t * txt_age;
		 const wchar_t * txt_data1;
		 const wchar_t * txt_data2;
		 const wchar_t * txt_data3;
		 const wchar_t * txt_data4;
		 const wchar_t * txt_gender;
		 const wchar_t * txt_hobby;
		 const wchar_t * txt_name;
		}name;

		class _id{
		public:
		const static int _name_start	=	65535;
		const static int btn_close	=	65536;
		const static int btn_delete	=	65541;
		const static int btn_insert	=	65540;
		const static int btn_max	=	65537;
		const static int btn_min	=	65539;
		const static int btn_modify	=	65542;
		const static int btn_ok	=	65578;
		const static int btn_restore	=	65538;
		const static int btn_sel_col	=	65543;
		const static int btn_test	=	65545;
		const static int btn_tv_test	=	65546;
		const static int chk_age	=	65580;
		const static int chk_gender	=	65579;
		const static int chk_hobby	=	65581;
		const static int chk_name	=	65558;
		const static int chk_sel	=	65556;
		const static int col_age	=	65562;
		const static int col_data1	=	65566;
		const static int col_data2	=	65568;
		const static int col_data3	=	65570;
		const static int col_data4	=	65572;
		const static int col_gender	=	65560;
		const static int col_hobby	=	65564;
		const static int col_name	=	65557;
		const static int container	=	65549;
		const static int edit_age	=	65576;
		const static int edit_gender	=	65575;
		const static int edit_hobby	=	65577;
		const static int edit_name	=	65574;
		const static int mclv_header	=	65548;
		const static int mclv_test	=	65547;
		const static int settingStyle_coWidth	=	65554;
		const static int settingStyle_customtitle	=	65553;
		const static int settingStyle_index	=	65551;
		const static int settingStyle_listview	=	65550;
		const static int settingStyle_title	=	65552;
		const static int settingStyle_visible	=	65555;
		const static int treeveiw_test	=	65544;
		const static int txt_age	=	65563;
		const static int txt_data1	=	65567;
		const static int txt_data2	=	65569;
		const static int txt_data3	=	65571;
		const static int txt_data4	=	65573;
		const static int txt_gender	=	65561;
		const static int txt_hobby	=	65565;
		const static int txt_name	=	65559;
		}id;

		class _string{
		public:
		const static int title	=	0;
		const static int ver	=	1;
		}string;

		class _color{
		public:
		const static int blue	=	0;
		const static int gray	=	1;
		const static int green	=	2;
		const static int red	=	3;
		const static int white	=	4;
		}color;

	};

#ifdef R_IN_CPP
	 extern const _R R;
	 extern const _UIRES UIRES;
#else
	 extern const __declspec(selectany) _R & R = _R();
	 extern const __declspec(selectany) _UIRES & UIRES = _UIRES();
#endif//R_IN_CPP
}
