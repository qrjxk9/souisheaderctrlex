#include "StdAfx.h"
#include "SelColumnDlg.h"

CSelColumnDlg::CSelColumnDlg(void):SHostDialog(UIRES.LAYOUT.dlg_sel_col)
{
}

CSelColumnDlg::~CSelColumnDlg(void)
{
}

BOOL CSelColumnDlg::OnInitDialog(HWND wndFocus, LPARAM lInitParam)
{
	FindChildByID(R.id.chk_name)->SetCheck(m_bCheck[0]);
	FindChildByID(R.id.chk_gender)->SetCheck(m_bCheck[1]);
	FindChildByID(R.id.chk_age)->SetCheck(m_bCheck[2]);
	FindChildByID(R.id.chk_hobby)->SetCheck(m_bCheck[3]);
	return TRUE;
}

void CSelColumnDlg::OnOK()
{
	m_bCheck[0]=!!FindChildByID(R.id.chk_name)->IsChecked();
	m_bCheck[1]=!!FindChildByID(R.id.chk_gender)->IsChecked();
	m_bCheck[2]=!!FindChildByID(R.id.chk_age)->IsChecked();
	m_bCheck[3]=!!FindChildByID(R.id.chk_hobby)->IsChecked();
	EndDialog(IDOK);
}
