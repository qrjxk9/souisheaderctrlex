#pragma once

class CInputDlg : public SHostDialog
{
public:
	SStringT m_strName,m_strGender,m_strHobby;
	int		 m_nAge;

	CInputDlg(void);
	~CInputDlg(void);

protected:
	void OnOK();
	EVENT_MAP_BEGIN()
		EVENT_NAME_COMMAND(L"btn_ok",OnOK)
	EVENT_MAP_END()

protected:
	BOOL OnInitDialog(HWND wndFocus, LPARAM lInitParam);
	BEGIN_MSG_MAP_EX(CInputDlg)
		MSG_WM_INITDIALOG(OnInitDialog)
		CHAIN_MSG_MAP(SHostWnd)
		REFLECT_NOTIFICATIONS_EX()
	END_MSG_MAP()

};
