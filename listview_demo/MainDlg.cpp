// MainDlg.cpp : implementation of the CMainDlg class
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "MainDlg.h"	
#include <helper/SAdapterBase.h>
#include "InputDlg.h"
#include "SelColumnDlg.h"

class CTestMcAdapterFix : public SMcAdapterBase
{
public:
	struct USERINFO
	{
		SStringT strName;
		SStringT strGender;
		int		 nAge;
		SStringT strHobby;
		BOOL	bChecked;
	};

	SArray<USERINFO> m_softInfo;

	bool			m_fVisible[4];//total 4 columns.
public:
	CTestMcAdapterFix()
	{
		for(int i=0;i<4;i++)
		{
			m_fVisible[i]=true;
		}
		for (int i = 0; i < 8; i++)
		{
			USERINFO info={SStringT().Format(_T("小白%d#"),i+1),_T("男"),18+i,_T("红警"),FALSE};
			m_softInfo.Add(info);
		}
	}

	virtual int getCount() override
	{
		return m_softInfo.GetCount();
	}


	virtual void getView(int position, SWindow * pItem, pugi::xml_node xmlTemplate) override
	{
		if (pItem->GetChildrenCount() == 0)
		{
			pItem->InitFromXml(xmlTemplate);
		}

		USERINFO *psi = m_softInfo.GetData() + position%m_softInfo.GetCount();
		pItem->FindChildByName(L"txt_name")->SetWindowText(S_CW2T(psi->strName));
		pItem->FindChildByName(L"txt_gender")->SetWindowText(S_CW2T(psi->strGender));
		pItem->FindChildByName(L"txt_age")->SetWindowText(SStringT().Format(_T("%d"), psi->nAge));
		pItem->FindChildByName(L"txt_hobby")->SetWindowText(psi->strHobby);
		SCheckBox *pCheck=pItem->FindChildByName2<SCheckBox>(L"chk_name");
		pCheck->SetCheck(psi->bChecked);
		pCheck->GetEventSet()->subscribeEvent(EventSwndStateChanged::EventID,Subscriber(&CTestMcAdapterFix::onCheckChanged,this));
		
	}

	bool onCheckChanged(EventArgs *e)
	{
		EventSwndStateChanged *e2=sobj_cast<EventSwndStateChanged>(e);
		if(e2->CheckState(WndState_Check))
		{
			SCheckBox *pCheck = sobj_cast<SCheckBox>(e->sender);
			//通过root的GetItemIndex获取点击列，不需要userdata.
			SItemPanel *pRoot = (SItemPanel*)pCheck->GetRoot();
			int iItem = pRoot->GetItemIndex();
			m_softInfo[iItem].bChecked=pCheck->IsChecked();
		}
		return true;
	}

	//获取每一列的列名的回调
	virtual SStringW GetColumnName(int iCol) const override{
		const WCHAR * kColNames[]={L"col_name",L"col_gender",L"col_age",L"col_hobby",L"col_data1",L"col_data2",L"col_data3",L"col_data4"};
		return kColNames[iCol];
	}

	virtual bool IsColumnVisible(int iCol) const
	{
		return m_fVisible[iCol];
	}

	void DeleteChecked()
	{
		for(int i=m_softInfo.GetCount()-1;i>=0;i--)
		{
			if(m_softInfo[i].bChecked)
				m_softInfo.RemoveAt(i);
		}
		notifyDataSetChanged();//注意:这里通知UI刷新界面。
	}

	void Insert(SStringT strName,SStringT strGender,int nAge,SStringT strHobby)
	{
		USERINFO si={strName,strGender,nAge,strHobby,FALSE};
		m_softInfo.Add(si);
		notifyDataSetChanged();//注意:这里通知UI刷新界面。
	}

	bool Modify(int iItem, SStringT strName,SStringT strGender,int nAge,SStringT strHobby)
	{
		if(iItem<0 || iItem>= m_softInfo.GetCount())
			return false;
		USERINFO si={strName,strGender,nAge,strHobby,FALSE};
		si.bChecked = m_softInfo[iItem].bChecked;
		m_softInfo[iItem]=si;
		notifyItemDataChanged(iItem);//注意:只刷新选中行。
		return true;
	}

	void CheckAll(BOOL bCheck)
	{
		for(int i=m_softInfo.GetCount()-1;i>=0;i--)
		{
			m_softInfo[i].bChecked=bCheck;
		}
		notifyDataSetChanged();//注意:这里通知UI刷新界面。
	}

	USERINFO Get(int iItem) const
	{
		SASSERT(iItem>=0 && iItem<m_softInfo.GetCount());
		return m_softInfo[iItem];
	}

	void SetColumnVisible(int iCol,bool bVisible=true)
	{
		if(iCol<0)
		{
			for(int i=0;i<4;i++)
			{
				m_fVisible[i]=bVisible;
			}
		}else
		{
			m_fVisible[iCol]=bVisible;
		}
	}

};


////////////////////////////////////////////////////////
CMainDlg::CMainDlg() : SHostWnd(_T("LAYOUT:XML_MAINWND"))
{
}

CMainDlg::~CMainDlg()
{
}

BOOL CMainDlg::OnInitDialog(HWND hWnd, LPARAM lParam)
{
	SMCListView *pLv = FindChildByName2<SMCListView>(L"mclv_test");
	SASSERT(pLv);
	CTestMcAdapterFix* myAdapter = new CTestMcAdapterFix();
	myAdapter->SetColumnVisible(0,true);
	myAdapter->SetColumnVisible(1,true);
	m_adapter.Attach(myAdapter);
	pLv->SetAdapter(m_adapter);

	pTreeViewTest = FindChildByName2<STreeView>(L"treeveiw_test");
	if (pTreeViewTest)
	{
		CTreeViewAdapter * pTreeViewAdapter = new CTreeViewAdapter();
		m_AdapterTreeView.Attach(pTreeViewAdapter);
	}

	return 0;
}
//TODO:消息映射
void CMainDlg::OnClose()
{
	SNativeWnd::DestroyWindow();
}

void CMainDlg::OnMaximize()
{
	SendMessage(WM_SYSCOMMAND, SC_MAXIMIZE);
}
void CMainDlg::OnRestore()
{
	SendMessage(WM_SYSCOMMAND, SC_RESTORE);
}
void CMainDlg::OnMinimize()
{
	SendMessage(WM_SYSCOMMAND, SC_MINIMIZE);
}

void CMainDlg::OnSize(UINT nType, CSize size)
{
	SetMsgHandled(FALSE);	
	SWindow *pBtnMax = FindChildByName(L"btn_max");
	SWindow *pBtnRestore = FindChildByName(L"btn_restore");
	if(!pBtnMax || !pBtnRestore) return;
	
	if (nType == SIZE_MAXIMIZED)
	{
		pBtnRestore->SetVisible(TRUE);
		pBtnMax->SetVisible(FALSE);
	}
	else if (nType == SIZE_RESTORED)
	{
		pBtnRestore->SetVisible(FALSE);
		pBtnMax->SetVisible(TRUE);
	}
}

//自动布局表头上的checkbox位置
void CMainDlg::OnMcLvHeaderRelayout(EventArgs *e)
{
	SHeaderCtrl *pHeader = sobj_cast<SHeaderCtrl>(e->sender);
	int nItems = pHeader->GetItemCount();
	SWindow *pChk = pHeader->FindChildByName(L"chk_sel");
	SASSERT(pChk);
	CRect rc = pHeader->GetItemRect(pHeader->GetOriItemIndex(0));

	CSize szChk = pChk->GetDesiredSize(-1,-1);
	CRect rc2(CPoint(rc.left + 5, rc.top + (rc.Height()-szChk.cy)/2), szChk);
	if (rc2.right >= rc.right - 5) rc2.right = rc.right - 5;
	pChk->Move(rc2);
}

void CMainDlg::OnDelete()
{
	if(SMessageBox(m_hWnd,_T("确定删除所有选中行吗?"),_T("提示"),MB_OKCANCEL|MB_ICONQUESTION)==IDOK)
	{
		m_adapter->DeleteChecked();
	}
}

void CMainDlg::OnInsert()
{
	CInputDlg inputDlg;
	inputDlg.m_strName=_T("SOUI");
	inputDlg.m_strGender=_T("unknown");
	inputDlg.m_nAge = 10;
	if(inputDlg.DoModal()==IDOK)
	{
		m_adapter->Insert(inputDlg.m_strName,inputDlg.m_strGender,inputDlg.m_nAge,inputDlg.m_strHobby);
	}
}

void CMainDlg::OnModify()
{
	SMCListView *pLv = FindChildByName2<SMCListView>(L"mclv_test");
	SASSERT(pLv);
	int iItem = pLv->GetSel();
	if(iItem == -1)
	{
		SMessageBox(m_hWnd,L"没有选中行",L"error",MB_OK);
		return;
	}
	CTestMcAdapterFix::USERINFO ui = m_adapter->Get(iItem);
	CInputDlg inputDlg;
	inputDlg.m_strName=ui.strName;
	inputDlg.m_strGender=ui.strGender;
	inputDlg.m_nAge = ui.nAge;
	if(inputDlg.DoModal()==IDOK)
	{
		m_adapter->Modify(iItem,inputDlg.m_strName,inputDlg.m_strGender,inputDlg.m_nAge,inputDlg.m_strHobby);
	}
}

void CMainDlg::OnCheckAll()
{
	SWindow *pCheck = FindChildByName(L"chk_sel");
	m_adapter->CheckAll(pCheck->IsChecked());
}

void CMainDlg::OnSelCol()
{
	SMCListView *pLv = FindChildByName2<SMCListView>(L"mclv_test");
	SHeaderCtrl2 *pHeader = sobj_cast<SHeaderCtrl2>(pLv->GetHeaderCtrl());
	SASSERT(pHeader);
	pHeader->ShowItemSelDialog();
	return;

	CSelColumnDlg dlg;
	memcpy(dlg.m_bCheck,m_adapter->m_fVisible,4*sizeof(bool));
	if(dlg.DoModal()==IDOK)
	{
		memcpy(m_adapter->m_fVisible,dlg.m_bCheck,4*sizeof(bool));
		m_adapter->notifyDataSetChanged();

		SWindow *pChk = FindChildByName(L"chk_sel");
		pChk->SetVisible(m_adapter->m_fVisible[0],TRUE);

		//relayout check box.
		EventHeaderRelayout evt(pChk->GetParent());
		OnMcLvHeaderRelayout(&evt);
	}
}

void CMainDlg::OnTvTest()
{
	m_AdapterTreeView->Clear();
	SOUI::SStringT path = _T("C:\\Users\\Administrator\\Desktop\\图库");
	m_AdapterTreeView->addTree(path);


	pTreeViewTest->SetAdapter(m_AdapterTreeView);
}

