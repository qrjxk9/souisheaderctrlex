// MainDlg.h : interface of the CMainDlg class
//
/////////////////////////////////////////////////////////////////////////////
#pragma once
#include "SHeaderCtrl2.h"
#include "helper\SAdapterBase.h"

typedef struct _tagLocalFileInfo       // 本地图库 - 目录文件信息 
{
	TCHAR path[MAX_PATH];              // 所在目录
	TCHAR name[MAX_PATH];              // 文件名		
	TCHAR name_1[MAX_PATH];            // 文件名	
	TCHAR name_2[MAX_PATH];            // 文件名	
}LocalFileInfo, *LPLocalFileInfo;
typedef std::vector<LocalFileInfo> LocalFileInfoList;


class CTreeViewAdapter :public SOUI::STreeAdapterBase<LocalFileInfo>
{
public:
	CTreeViewAdapter() {}
	~CTreeViewAdapter() {}


	// 子方法 - 增加目录树 最多伸展10次
	void addTree(SOUI::SStringT& strCurDir, HSTREEITEM root = STVI_ROOT, int deep = 10)
	{
		SOUI::SStringT strDir = strCurDir,strNextDir;
		strDir.Append(_T("\\*"));
		if(deep-- <= 0) return; // 搜索深度

		LocalFileInfo fi;
		WIN32_FIND_DATA fd;
		HANDLE hDir = ::FindFirstFile(strDir.GetBuffer(0), &fd);
		if (hDir == INVALID_HANDLE_VALUE) return ;
		if(root == STVI_ROOT){
			TCHAR* name = _tcsrchr(strCurDir.GetBuffer(0),_T('\\'));
			lstrcpy(fi.path, strCurDir.GetBuffer(0));
			lstrcpy(fi.name, name + 1);
			root = InsertItem(fi);
			ExpandItem(root, SOUI::ITvAdapter::TVC_TOGGLE);//展开
		}
		do{
			strNextDir = strCurDir;
			strNextDir.Append(_T("\\"));
			if (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY){
				SOUI::SStringT strFileName = fd.cFileName;
				if (strFileName.Compare(_T(".")) == 0 || strFileName.Compare(_T("..")) == 0) 
					continue;

				strNextDir.Append(fd.cFileName);
				lstrcpy(fi.name, fd.cFileName);
				lstrcpy(fi.path, strNextDir.GetBuffer(0));

				HSTREEITEM hRoot = InsertItem(fi, root);
				this->addTree(strNextDir, hRoot, deep);
				ExpandItem(hRoot, SOUI::ITvAdapter::TVC_TOGGLE);//展开
			}
		} while (::FindNextFile(hDir, &fd));
		::FindClose(hDir);
	}

	void Clear()
	{
		this->DeleteItem(STVI_ROOT);
	}

	virtual void getView(SOUI::HTREEITEM loc, SOUI::SWindow * pItem, pugi::xml_node xmlTemplate) {
		if (pItem->GetChildrenCount() == 0)
		{
			pItem->InitFromXml(xmlTemplate);
		}
		ItemInfo & ii = m_tree.GetItemRef((HSTREEITEM)loc);
		pItem->SetUserData((ULONG_PTR)&(ii.data));
		SOUI::SWindow * pWnd = pItem->FindChildByName(L"btn_test");
		SASSERT(pWnd);
		pWnd->SetWindowText(SOUI::S_CW2T(ii.data.name));
		pWnd->SetAttribute(L"tip",SOUI::S_CW2T(ii.data.name));//添加tip
	}

	bool OnSwitchClick(SOUI::EventArgs *pEvt)
	{
		SOUI::SToggle *pToggle = SOUI::sobj_cast<SOUI::SToggle>(pEvt->sender);
		SASSERT(pToggle);
		SOUI::SItemPanel *pItem = SOUI::sobj_cast<SOUI::SItemPanel>(pToggle->GetRoot());
		SASSERT(pItem);
		SOUI::HTREEITEM loc = (SOUI::HTREEITEM)pItem->GetItemIndex();
		ExpandItem(loc, ITvAdapter::TVC_TOGGLE);
		return true;
	}
};



class CTestMcAdapterFix;
class CMainDlg : public SHostWnd
{
	SAutoRefPtr<CTestMcAdapterFix> m_adapter;
	SOUI::SAutoRefPtr<CTreeViewAdapter> m_AdapterTreeView;
public:
	CMainDlg();
	~CMainDlg();

	void OnClose();
	void OnMaximize();
	void OnRestore();
	void OnMinimize();
	void OnSize(UINT nType, CSize size);
	BOOL OnInitDialog(HWND wndFocus, LPARAM lInitParam);

protected:
	void OnMcLvHeaderRelayout(EventArgs *e);
	void OnInsert();
	void OnDelete();
	void OnModify();
	void OnCheckAll();
	void OnSelCol();

	void OnTvTest();
	//soui消息
	EVENT_MAP_BEGIN()
		EVENT_NAME_HANDLER(L"mclv_header",EventHeaderRelayout::EventID,OnMcLvHeaderRelayout)
		EVENT_NAME_COMMAND(L"btn_close", OnClose)
		EVENT_NAME_COMMAND(L"btn_min", OnMinimize)
		EVENT_NAME_COMMAND(L"btn_max", OnMaximize)
		EVENT_NAME_COMMAND(L"btn_restore", OnRestore)
		EVENT_NAME_COMMAND(L"btn_insert",OnInsert)
		EVENT_NAME_COMMAND(L"btn_delete",OnDelete)
		EVENT_NAME_COMMAND(L"btn_modify",OnModify)
		EVENT_NAME_COMMAND(L"chk_sel",OnCheckAll)
		EVENT_ID_COMMAND(R.id.btn_sel_col,OnSelCol)

		EVENT_NAME_COMMAND(L"btn_tv_test",OnTvTest)
	EVENT_MAP_END()
		
	//HostWnd真实窗口消息处理
	BEGIN_MSG_MAP_EX(CMainDlg)
		MSG_WM_INITDIALOG(OnInitDialog)
		MSG_WM_CLOSE(OnClose)
		MSG_WM_SIZE(OnSize)
		CHAIN_MSG_MAP(SHostWnd)
		REFLECT_NOTIFICATIONS_EX()
	END_MSG_MAP()

private:
	SOUI::STreeView*			pTreeViewTest;

};
