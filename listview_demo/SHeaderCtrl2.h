#pragma once

namespace SOUI{
	class SHeaderCtrl2 : public SHeaderCtrl
	{
		SOUI_CLASS_NAME(SHeaderCtrl2,L"header2")
	public:
		SHeaderCtrl2(void);
		~SHeaderCtrl2(void);

	public:
		void ShowItemSelDialog();
	protected:
		BOOL CreateChildren(pugi::xml_node xmlNode) override;

		pugi::xml_document m_xmlDialog;
	};

}
