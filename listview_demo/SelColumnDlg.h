#pragma once

class CSelColumnDlg : public SHostDialog
{
public:
	CSelColumnDlg(void);
	~CSelColumnDlg(void);
	bool	m_bCheck[4];

protected:
	void OnOK();
	EVENT_MAP_BEGIN()
		EVENT_NAME_COMMAND(L"btn_ok",OnOK)
	EVENT_MAP_END()

protected:
	BOOL OnInitDialog(HWND wndFocus, LPARAM lInitParam);
	BEGIN_MSG_MAP_EX(CSelColumnDlg)
		MSG_WM_INITDIALOG(OnInitDialog)
		CHAIN_MSG_MAP(SHostWnd)
	END_MSG_MAP()
};
