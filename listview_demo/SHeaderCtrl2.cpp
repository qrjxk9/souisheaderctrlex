#include "StdAfx.h"
#include "SHeaderCtrl2.h"
#include "helper\SAdapterBase.h"

namespace SOUI{

	class CTestAdapterFixHorz : public SAdapterBase
	{
	public:
		struct SettingInfo
		{
			SStringT	strTitleName;				// 列名
			SStringT	customTitleName;			// 自定义列名
			int			colWidth;					// 列宽
			int			colOrder;					// 列的顺序
			bool		bVisible;					// 列是否隐藏
		};

		SArray<SettingInfo> m_softInfo;
	public:
		CTestAdapterFixHorz(){}

		~CTestAdapterFixHorz(){}

		void SetData(const std::vector<SettingInfo>& arrInfo)
		{
			m_softInfo.RemoveAll();
			for(size_t i=0;i<arrInfo.size();++i)
				m_softInfo.Add(arrInfo.at(i));
			notifyDataSetChanged();
		}

		virtual int getCount()
		{
			return m_softInfo.GetCount();
		}

		//初始化编辑框
		void InitEdit(SOUI::SWindow* pItem,SOUI::SStringT editName,SOUI::SStringT strContent,int position)
		{
			SOUI::SEdit* pEdit = pItem->FindChildByName2<SOUI::SEdit>(editName/*L"edit_BlkName"*/);

			pEdit->GetEventSet()->setMutedState(true);
			pEdit->SetWindowText(strContent);//这里使用呢称
			pEdit->GetEventSet()->setMutedState(false);
			pEdit->GetEventSet()->subscribeEvent(SOUI::EVT_RE_NOTIFY,Subscriber(&CTestAdapterFixHorz::OnReNotify,this));
			pEdit->GetEventSet()->subscribeEvent(SOUI::EVT_CTXMENU, Subscriber(&CTestAdapterFixHorz::OnEditRLbuttonDownMutex, this));//屏蔽SEdit自带的右键菜单
			pEdit->SetUserData(position);
		}

		virtual void getView(int position, SWindow * pItem, pugi::xml_node xmlTemplate)
		{
			if (pItem->GetChildrenCount() == 0)
			{
				pItem->InitFromXml(xmlTemplate);
			}

			SettingInfo *psi = m_softInfo.GetData() + position%m_softInfo.GetCount();
			pItem->FindChildByName(L"settingStyle_index")->SetWindowText(SOUI::SStringT().Format(_T("%d"),position));
			if(_T("")!=psi->strTitleName)
				pItem->FindChildByName(L"settingStyle_title")->SetWindowText(psi->strTitleName);
			else
				pItem->FindChildByName(L"settingStyle_title")->SetWindowText(_T("        "));
			InitEdit(pItem,_T("settingStyle_customtitle"),psi->customTitleName,position);
			InitEdit(pItem,_T("settingStyle_coWidth"),SOUI::SStringT().Format(_T("%d"),psi->colWidth),position);

			SOUI::SCheckBox* pCheck = pItem->FindChildByName2<SOUI::SCheckBox>(L"settingStyle_visible");
			if(pCheck)pCheck->SetCheck(psi->bVisible);

			pCheck->GetEventSet()->subscribeEvent(SOUI::EVT_CMD, Subscriber(&CTestAdapterFixHorz::OnCheckClick, this));
		}

		//屏蔽Edit的默认右键菜单
		bool OnEditRLbuttonDownMutex(SOUI::EventArgs * pEvt)
		{   
			SOUI::EventCtxMenu* pEvtCtxMenu = (SOUI::EventCtxMenu*)pEvt;  
			pEvtCtxMenu->bCancel = TRUE;
			return true;
		}

		bool OnReNotify(SOUI::EventArgs *e)
		{
			SOUI::EventRENotify * pItem = SOUI::sobj_cast<SOUI::EventRENotify>(e);
			SASSERT(pItem);
			if(pItem->iNotify == EN_CHANGE)
			{
				SOUI::SEdit* pTmp = SOUI::sobj_cast<SOUI::SEdit>(pItem->sender);
				int iItem = (int)pTmp->GetUserData();
				if (pItem->nameFrom != NULL && wcscmp(pItem->nameFrom, L"settingStyle_customtitle") == 0)//块名呢称
				{
					SOUI::SStringT str = pTmp->GetWindowText();
					m_softInfo.GetAt(iItem).customTitleName = str;
				}
				else if (pItem->nameFrom != NULL && wcscmp(pItem->nameFrom, L"settingStyle_coWidth") == 0)//数量
				{
					m_softInfo.GetAt(iItem).colWidth = _tstof(pTmp->GetWindowText());
				}
			}
			return true;
		}

		bool OnCheckClick(SOUI::EventArgs * pEvt)
		{
			SCheckBox *pCheck = sobj_cast<SCheckBox>(pEvt->sender);
			SItemPanel *pItem = (SItemPanel*)pCheck->GetRoot();
			int iItem = pItem->GetItemIndex();
			if(iItem>-1 && iItem<m_softInfo.GetCount())
			{
				m_softInfo.GetAt(iItem).bVisible = pCheck->IsChecked();
			}
			return true;
		}
	};

	struct OptInfo{
		SStringT strName;
		BOOL bChecked;
	};

	class SOptDialog :public SHostDialog{
	public:
		SOptDialog(pugi::xml_node rootNode,SArray<OptInfo>* optInfo):SHostDialog(L""){
			m_rootNode = rootNode;
			m_info = optInfo;
		}
	protected:
		static const int kIdStart = 100;
		BOOL OnLoadLayoutFromResourceID(const SStringT &resId) override{
			if(!InitFromXml(m_rootNode))
				return FALSE;
			pugi::xml_document xmlContent;
			pugi::xml_node root = xmlContent.append_child(L"window");
			root.append_attribute(L"pos").set_value(L"0,0,-0,-0");
			for(int i=0;i<m_info->GetCount();i++){
				pugi::xml_node item = root.append_child(SCheckBox::GetClassName());
				item.append_attribute(L"text").set_value(S_CT2W(m_info->GetAt(i).strName));
				item.append_attribute(L"id").set_value(i+kIdStart);//id range from [100,100+nItem)
				item.append_attribute(L"checked").set_value(m_info->GetAt(i).bChecked);
			}
			FindChildByName(L"container")->CreateChildren(root);

			SListView *pLstViewFixHorz = FindChildByName2<SListView>("settingStyle_listview");
			if(pLstViewFixHorz)
			{
				CTestAdapterFixHorz* myAdapter = new CTestAdapterFixHorz();
				m_ExportDetialadapter.Attach(myAdapter);
				pLstViewFixHorz->SetAdapter(m_ExportDetialadapter);

				std::vector<CTestAdapterFixHorz::SettingInfo> arrInfo;
				for(int i=0;i<m_info->GetCount();i++)
				{
					CTestAdapterFixHorz::SettingInfo info;
					info.strTitleName = m_info->GetAt(i).strName;
					arrInfo.push_back(info);
				}
				m_ExportDetialadapter->SetData(arrInfo);
			}

			return TRUE;
		}

		BOOL OnInitDialog(HWND wndFocus, LPARAM lInitParam){

			return TRUE;
		}

		void OnItemChanged(int nID){
			SWindow *pWnd = FindChildByID(nID);
			int iItem = nID - kIdStart;
			(*m_info)[iItem].bChecked = pWnd->IsChecked();
		}

		EVENT_MAP_BEGIN()
			EVENT_ID_COMMAND_RANGE(kIdStart,kIdStart+m_info->GetCount(),OnItemChanged)
		EVENT_MAP_END()

		BEGIN_MSG_MAP_EX(SOptDialog)
			MSG_WM_INITDIALOG(OnInitDialog)
			CHAIN_MSG_MAP(SHostDialog)
		END_MSG_MAP()

	private:
		pugi::xml_node m_rootNode;
		SArray<OptInfo> *m_info;
		SOUI::SAutoRefPtr<CTestAdapterFixHorz> m_ExportDetialadapter;
	};

	SHeaderCtrl2::SHeaderCtrl2(void)
	{
	}

	SHeaderCtrl2::~SHeaderCtrl2(void)
	{
	}

	BOOL SHeaderCtrl2::CreateChildren(pugi::xml_node xmlNode)
	{
		pugi::xml_node selItemTemp = xmlNode.child(L"selItemTemp");
		if(!selItemTemp) return FALSE;
		m_xmlDialog.append_copy(selItemTemp);
		return __super::CreateChildren(xmlNode);
	}

	void SHeaderCtrl2::ShowItemSelDialog()
	{
		SArray<OptInfo> optInfo;
		for(int i=0;i<GetItemCount();i++){
			SHDITEM item;
			item.mask = SHDI_TEXT;
			GetItem(i,&item);
			OptInfo info;
			info.strName = item.strText.GetText(TRUE);			
			info.bChecked = FALSE;//todo: init to false here
			optInfo.Add(info);
		}
		SOptDialog optDlg(m_xmlDialog.root().first_child(),&optInfo);
		optDlg.DoModal(GetContainer()->GetHostHwnd());
		for(int i=0;i<optInfo.GetCount();i++){
			SLOG_INFO("item "<< optInfo[i].strName.c_str()<<" checked:"<<optInfo[i].bChecked);
		}
	}

}
