#include "StdAfx.h"
#include "InputDlg.h"

CInputDlg::CInputDlg(void):SHostDialog(L"LAYOUT:dlg_input")
{
}

CInputDlg::~CInputDlg(void)
{
}


BOOL CInputDlg::OnInitDialog(HWND wndFocus, LPARAM lInitParam)
{
	FindChildByName(L"edit_name")->SetWindowText(m_strName);
	FindChildByName(L"edit_gender")->SetWindowText(m_strGender);
	FindChildByName(L"edit_age")->SetWindowText(SStringT().Format(_T("%d"),m_nAge));
	FindChildByName(L"edit_hobby")->SetWindowText(m_strHobby);
	return TRUE;
}


void CInputDlg::OnOK()
{
	m_strName =FindChildByName(L"edit_name")->GetWindowText();
	m_strGender=FindChildByName(L"edit_gender")->GetWindowText();
	SStringT strAge = FindChildByName(L"edit_age")->GetWindowText();
	m_nAge = _ttoi(strAge);
	m_strHobby = FindChildByName(L"edit_hobby")->GetWindowText();
	EndDialog(IDOK);
}